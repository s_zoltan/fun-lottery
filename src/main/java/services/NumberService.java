package main.java.services;

import java.util.TreeSet;

public interface NumberService {
    TreeSet<Integer> generateNumbers();
}
