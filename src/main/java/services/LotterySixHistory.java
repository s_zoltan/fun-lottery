package main.java.services;

import main.java.model.LotterySix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class LotterySixHistory {
    public ArrayList<LotterySix> Load() {

        ArrayList<LotterySix> lottolist = new ArrayList<>();
        String line;
        try {
            URL url = new URL("https://bet.szerencsejatek.hu/cmsfiles/hatos.csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = br.readLine()) != null) {
                String[] splitline = line.split(";");
                LotterySix addToList = new LotterySix(splitline);
                lottolist.add(addToList);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lottolist;
    }
}
