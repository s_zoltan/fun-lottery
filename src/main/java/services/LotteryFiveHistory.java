package main.java.services;

import main.java.model.LotteryFive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class LotteryFiveHistory {
    public ArrayList<LotteryFive> Load() {

        ArrayList<LotteryFive> lottolist = new ArrayList<>();
        String line;
        try {
            URL url = new URL("https://bet.szerencsejatek.hu/cmsfiles/otos.csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = br.readLine()) != null) {
                String[] splitline = line.split(";");
                LotteryFive addToList = new LotteryFive(splitline);
                lottolist.add(addToList);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lottolist;
    }


}
