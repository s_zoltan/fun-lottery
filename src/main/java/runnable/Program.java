package main.java.runnable;
import main.java.model.*;
import main.java.services.LotteryFiveHistory;
import main.java.services.LotterySixHistory;
import main.java.tools.*;



import java.io.*;
import java.util.ArrayList;
import java.util.TreeSet;


public class Program {


    public static void main(String[] args) throws IOException {
        LotteryFiveHistory lotteryFiveHistory = new LotteryFiveHistory();
        LotterySixHistory lotterySixHistory = new LotterySixHistory();

        CertificateTools certificateFix = CertificateTools.getInstance();
        certificateFix.fixHTTPSissue();

        ArrayList<LotteryFive> lottolistfive = lotteryFiveHistory.Load();
        ArrayList<LotterySix> lottolistsix = lotterySixHistory.Load();

        WelcomeMessage welcomeMessage = new WelcomeMessage(lottolistfive,lottolistsix);
        welcomeMessage.message();


        LotteryFiveGenerator generateFive = new LotteryFiveGenerator(lottolistfive);
        TreeSet<Integer> numbersfive = generateFive.generateNumbers();


        LotterySixGenerator generateSix = new LotterySixGenerator(lottolistsix);
        TreeSet<Integer> numberssix = generateSix.generateNumbers();
        System.out.println("Your lucky numbers for next week are: "+numbersfive + " and " + numberssix);


    }

}