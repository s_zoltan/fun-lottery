package main.java.tools;

import main.java.model.LotteryFive;
import main.java.model.LotterySix;


import java.util.ArrayList;

public class WelcomeMessage {
    private ArrayList<LotteryFive> listOfFive;
    private ArrayList<LotterySix> listOfSix;

    public WelcomeMessage(ArrayList<LotteryFive> list1, ArrayList<LotterySix> list2) {
        listOfFive = list1;
        listOfSix = list2;
    }

    public void message() {
        System.out.println("#####################################################################################");
        System.out.println("#              __      _____ _    ___ ___  __  __ ___   _____ ___                   #");
        System.out.println("#              \\ \\    / / __| |  / __/ _ \\|  \\/  | __| |_   _/ _ \\                  #");
        System.out.println("#               \\ \\/\\/ /| _|| |_| (_| (_) | |\\/| | _|    | || (_) |                 #");
        System.out.println("#                \\_/\\_/ |___|____\\___\\___/|_|  |_|___|   |_| \\___/                  #");
        System.out.println("#                                                                                   #");
        System.out.println("#  _    ___ _____ _____ ___ ___ __   __   ___ ___ _  _ ___ ___    _ _____ ___  ___  #");
        System.out.println("# | |  / _ \\_   _|_   _| __| _  \\ \\ / /  / __| __| \\| | __| _ \\  /_\\_   _/ _ \\| _ \\ #");
        System.out.println("# | |_| (_) || |   | | | _||   / \\ V /  | (_ | _|| .` | _||   / / _ \\| || (_) |   / #");
        System.out.println("# |____\\___/ |_|   |_| |___|_|_\\  |_|    \\___|___|_|\\_|___|_|_\\/_/ \\_\\_| \\___/|_|_\\ #");
        System.out.println("#                                                                                   #");
        System.out.println("#####################################################################################\n");

        System.out.println("Winning numbers for five-number draw last week were: "+listOfFive.get(0).getNumbers());
        System.out.println("Winning numbers for six-number draw last week were: "+listOfSix.get(0).getNumbers());
    }





}
