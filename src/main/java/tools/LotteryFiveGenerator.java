package main.java.tools;

import main.java.model.LotteryFive;
import main.java.services.NumberService;

import java.util.ArrayList;
import java.util.Random;
import java.util.TreeSet;

public class LotteryFiveGenerator implements NumberService {
    private ArrayList<LotteryFive> list;
    private TreeSet<Integer> numbers = new TreeSet<>();


    public LotteryFiveGenerator(ArrayList<LotteryFive> list) {
        this.list = list;
    }

    public TreeSet<Integer> generateNumbers() {
        generateRandom();
        for (int i=0; i<list.size(); i++) {
            if(list.get(i).getNumbers().equals(numbers)) {
                i=-1;
                generateRandom();
            }
        }
        return numbers;
    }

    private void generateRandom() {
        Random rand = new Random();
        numbers.clear();
        do {
            numbers.add(rand.nextInt(90) + 1);
        } while (numbers.size() < 5);
    }
}
