package main.java.tools;

import main.java.model.LotteryFive;
import main.java.model.LotterySix;
import main.java.services.NumberService;

import java.util.ArrayList;
import java.util.Random;
import java.util.TreeSet;

public class LotterySixGenerator implements NumberService {
    private ArrayList<LotterySix> list;
    private TreeSet<Integer> numbers = new TreeSet<>();

    public LotterySixGenerator(ArrayList<LotterySix> list) {
        this.list = list;
    }

    public TreeSet<Integer>  generateNumbers() {
        generateRandom();

        for (int i=0; i<list.size(); i++) {
            if(list.get(i).getNumbers().containsAll(numbers)) {
                i=-1;
                generateRandom();

            }
        }
        return numbers;
    }

    private void generateRandom() {
        Random rand = new Random();
        numbers.clear();
        do {
            numbers.add(rand.nextInt(45) + 1);
        } while (numbers.size() < 6);
    }


}
