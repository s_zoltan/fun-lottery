package main.java.model;

import main.java.model.LotteryFive;

import java.util.Random;

public class LotterySix extends LotteryFive {

    public boolean legacy = false;

    //load from file
    public LotterySix(String[] line) {
        super(line);
        try {
            numbers.add(Integer.parseInt(line[line.length - 6]));
        } catch (Exception e) {
            System.out.println("Error, files not found, or wrong format");
        }

        //check the quantity of the numbers at the end of the line. Till 2007 an additional number was drawn.
        if (line[line.length-7].matches("^[0-9]+$")) {
            numbers.add(Integer.parseInt(line[line.length - 7]));
            legacy = true;
        }

    }

    //create random
    public LotterySix() {
        super();
        numbers.clear();
        Random rand = new Random();
        do {
            numbers.add(rand.nextInt(45) + 1);
        } while (numbers.size() < 6);
    }

    //constructor for testing
    public LotterySix(int a, int b, int c, int d, int e, int f) {
        super(a,b,c,d,e);
        numbers.add(f);
    }

    @Override
    public String toString() {
        return "main.java.model.LotterySix{" +
                "year='" + year + '\'' +
                ", week='" + week + '\'' +
                ", numbers=" + numbers +
                '}';
    }
}
