package main.java.model;

import java.util.Random;
import java.util.TreeSet;

public class LotteryFive{
    String year;
    String week;
    TreeSet<Integer> numbers = new TreeSet<>();

    public LotteryFive(String[] line) {
        try {
            year = line[0];
            week = line[1];
            numbers.add(Integer.parseInt(line[line.length-5]));
            numbers.add(Integer.parseInt(line[line.length-4]));
            numbers.add(Integer.parseInt(line[line.length-3]));
            numbers.add(Integer.parseInt(line[line.length-2]));
            numbers.add(Integer.parseInt(line[line.length-1]));
        }
        catch (Exception e) {
            System.out.println("Error, files not found, or wrong format");
        }

    }

    //create random
    public LotteryFive() {
        Random rand = new Random();
        do {
            numbers.add(rand.nextInt(90) + 1);
        } while (numbers.size() < 5);
    }

    //constructor for testing
    public LotteryFive(int a, int b, int c, int d, int e) {
        numbers.add(a);
        numbers.add(b);
        numbers.add(c);
        numbers.add(d);
        numbers.add(e);
    }

    @Override
    public String toString() {
        return "main.java.model.LotteryFive{" +
                "year='" + year + '\'' +
                ", week='" + week + '\'' +
                ", numbers=" + numbers +
                '}';
    }

    public TreeSet<Integer> getNumbers() {
        return numbers;
    }

    public String getWeek() {
        return week;
    }
}
